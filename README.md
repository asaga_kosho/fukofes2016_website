# README #
## 大阪教育大学附属高等学校天王寺校舎の2016年度附高祭ウェブサイトのコードを保管しています。

著作権は大阪教育大学附属高等学校天王寺校舎自治会に帰属します。

高61期　浅賀巧匠　asagatto777@gmail.com

Created by Kosho Asaga

Special Thanks to K.Inada, H.Ibaraki, C.Kitaguchi, C.Kittaka